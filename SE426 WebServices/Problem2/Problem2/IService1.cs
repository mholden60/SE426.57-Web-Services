﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
//using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml.XPath;

namespace Problem2
{
    [ServiceContract]
    public interface INEITStoreOrdersService
    {

        [OperationContract]
        int ReturnAmountEarned(float balance, float intrest);

        [OperationContract]
        string GetBankInformation(string AccountID);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class OrderInfo
    {
        string account;

        [DataMember]
        public string Account
        {
            get { return account; }
            set { account = value; }


        }
    }
}
