﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace mholdenLab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetElementByTagName_Click(object sender, EventArgs e)
        {
            string strFilename = Application.StartupPath + "\\ContactManager.xml";
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(strFilename);
            XmlElement elm = xmlDoc.DocumentElement;
            XmlNodeList version = xmlDoc.GetElementsByTagName("Version");
            XmlNodeList date = xmlDoc.GetElementsByTagName("Date");
            XmlNodeList last = xmlDoc.GetElementsByTagName("Last");
            XmlNodeList phone = xmlDoc.GetElementsByTagName("Phone");
            XmlNodeList fax = xmlDoc.GetElementsByTagName("Fax");
            XmlNodeList email = xmlDoc.GetElementsByTagName("Email");
            XmlNodeList gen = xmlDoc.SelectNodes("/ContactInformation/Contact");
            XmlNodeList gender = xmlDoc.SelectNodes("/ContactInformation/Contact/@sex");
            

            foreach (XmlNode node in version)
            {
                Console.WriteLine(node.InnerText);
            }
            foreach (XmlNode node in date)
            {
                Console.WriteLine(node.InnerText);
            }
            foreach (XmlNode node in gen)
            {
                if(node.Attributes[0].Value =="m")
                {
                    Console.WriteLine("Male " + node.InnerText);
                }

                else { Console.WriteLine("Female " + node.InnerText);
                }
               // Console.WriteLine(node.Attributes[0].Value + " " + node.InnerText);

            }
           
            
        }
    }
}
