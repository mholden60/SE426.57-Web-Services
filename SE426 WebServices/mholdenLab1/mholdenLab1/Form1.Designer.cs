﻿namespace mholdenLab1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GetElementByTagName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GetElementByTagName
            // 
            this.GetElementByTagName.Location = new System.Drawing.Point(49, 22);
            this.GetElementByTagName.Name = "GetElementByTagName";
            this.GetElementByTagName.Size = new System.Drawing.Size(143, 92);
            this.GetElementByTagName.TabIndex = 0;
            this.GetElementByTagName.Text = "Get Element by Tag Name";
            this.GetElementByTagName.UseVisualStyleBackColor = true;
            this.GetElementByTagName.Click += new System.EventHandler(this.GetElementByTagName_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.GetElementByTagName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GetElementByTagName;
    }
}

