﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
namespace Problem3._1
{
    public partial class Cate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            string directory = HttpContext.Current.Server.MapPath(".");
            string strFilename = directory + "\\" + "\\catalog.xml";
            docNav = new XPathDocument(strFilename);

            nav = docNav.CreateNavigator();
            NodeIter = nav.Select("/catalog");
            
            while(NodeIter.MoveNext())
            {
                XPathNodeIterator billingInfo = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtPrice.Text = txtPrice.Text + billingInfo.Current.SelectSingleNode("//catalog/product/catalog_item/price[.>40]");
               // txtPrice.Text = txtPrice.Text + billingInfo.Current.SelectSingleNode("//");
            }
        }
    }
}