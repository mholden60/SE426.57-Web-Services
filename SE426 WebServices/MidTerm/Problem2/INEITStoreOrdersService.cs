﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Problem2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface INEITStoreOrdersService
    {

        [OperationContract]
        int ReturnAmountEarned(float balance, float intrest);

        [OperationContract]
        string GetBankInformation(string AccountID);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class OrderInfo
    {
        string account;

        [DataMember]
        public string Account
        {
            get { return account; }
            set { account = value; }

                 
        }
    }
}
