﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;

namespace MidTerm
{
    public partial class Problem1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
            XmlTextReader myXMLReader = null;           //Declare XMLReader           
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            string Bank = "http://54.84.69.209/midtermBankingService/Bankservice.asmx";

            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(Bank);
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";
            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());

            docNav = new XPathDocument(myXMLReader);
            nav = docNav.CreateNavigator();
            NodeIter = nav.Select("//OwnerName");
            while(NodeIter.MoveNext())
            { 
            txtName.Text = txtName.Text + NodeIter.Current.SelectSingleNode("//OwnerName");
            txtID.Text = txtID.Text + NodeIter.Current.SelectSingleNode("//AccountID");
            }
        }
    }
}