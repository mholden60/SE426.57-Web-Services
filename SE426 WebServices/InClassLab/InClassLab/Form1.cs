﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;

namespace InClassLab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnVdVn_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator nodeIter;

            string xmlFile = Application.StartupPath + "\\ContactManager.xml";
            docNav = new XPathDocument(xmlFile);

            nav = docNav.CreateNavigator();

            nodeIter = nav.Select(cmbVdVn.Text);

            cmbVdVn.Text = "";

            while(nodeIter.MoveNext())
            {
                cmbVdVn.Items.Add(nodeIter.Current.Value);
                XPathNodeIterator kidNode;
                kidNode = nodeIter.Current.Select("Version");
                while (kidNode.MoveNext())
                    cmbVdVn.Items.Add(kidNode.Current.Value);

            }




        }

        
    }
}
