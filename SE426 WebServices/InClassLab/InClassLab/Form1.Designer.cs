﻿namespace InClassLab
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVdVn = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnMales = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.cmbVdVn = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnVdVn
            // 
            this.btnVdVn.Location = new System.Drawing.Point(175, 83);
            this.btnVdVn.Name = "btnVdVn";
            this.btnVdVn.Size = new System.Drawing.Size(158, 23);
            this.btnVdVn.TabIndex = 1;
            this.btnVdVn.Text = "Display Version Date/Time";
            this.btnVdVn.UseVisualStyleBackColor = true;
            this.btnVdVn.Click += new System.EventHandler(this.btnVdVn_Click);
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(175, 182);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(158, 23);
            this.btnAll.TabIndex = 3;
            this.btnAll.Text = "Display All Information";
            this.btnAll.UseVisualStyleBackColor = true;
            // 
            // btnMales
            // 
            this.btnMales.Location = new System.Drawing.Point(175, 281);
            this.btnMales.Name = "btnMales";
            this.btnMales.Size = new System.Drawing.Size(158, 23);
            this.btnMales.TabIndex = 5;
            this.btnMales.Text = "Display All Males";
            this.btnMales.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(175, 380);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // cmbVdVn
            // 
            this.cmbVdVn.FormattingEnabled = true;
            this.cmbVdVn.Location = new System.Drawing.Point(13, 14);
            this.cmbVdVn.Name = "cmbVdVn";
            this.cmbVdVn.Size = new System.Drawing.Size(406, 56);
            this.cmbVdVn.TabIndex = 9;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(14, 112);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(406, 56);
            this.listBox1.TabIndex = 10;
          //  this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(13, 211);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(406, 56);
            this.listBox2.TabIndex = 11;
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(13, 310);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(406, 56);
            this.listBox3.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 447);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.cmbVdVn);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnMales);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.btnVdVn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnVdVn;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnMales;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListBox cmbVdVn;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
    }
}

