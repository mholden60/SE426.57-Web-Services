﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mortgage.aspx.cs" Inherits="Mortage.Mortgage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblMonth" runat="server" Text="Month(Enter 3)"></asp:Label>
        <asp:TextBox ID="txtMonth" runat="server"></asp:TextBox>
    </div>
    <div>
        <asp:Label ID="lblYear" runat="server" Text="Year(Enter 2003)"></asp:Label>
        <asp:TextBox ID="txtYear" runat="server"></asp:TextBox>
    </div>
     <div>
         <asp:Button ID="btnRates" runat="server" Text="Get Rates" OnClick="btnRates_Click" />
    </div>
    <div>
        <asp:Label ID="lbl3Month" runat="server" Text="3 Month Treasury Bill: "></asp:Label>
    </div>
    <div>
        <asp:Label ID="lbl5Month" runat="server" Text="6 Month Treasury Bill: "></asp:Label>
    </div>
    </form>
</body>
</html>
