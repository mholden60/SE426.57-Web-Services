﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mortage
{
    public partial class Mortgage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRates_Click(object sender, EventArgs e)
        {
            Mortgage_WebService.MortgageIndex mort = new Mortgage_WebService.MortgageIndex();
            Mortgage_WebService.MonthlyIndex monthMort = mort.GetMortgageIndexByMonth(Convert.ToInt32(txtMonth.Text), Convert.ToInt32(txtYear.Text));
            lbl3Month.Text = monthMort.ThreeMonthTreasuryBill.ToString();
            lbl5Month.Text = monthMort.SixMonthTreasuryBill.ToString();



        }
    }
}