﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mortage.Startup))]
namespace Mortage
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
