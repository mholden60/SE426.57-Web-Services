﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetElementsByTagName_Click(object sender, EventArgs e)
        {
            string strFilename = Application.StartupPath + "\\SalesInvoice.xml";
            XmlDocument xmlDoc = new XmlDocument();


            xmlDoc.Load(strFilename);
            XmlElement elm = xmlDoc.DocumentElement;
            XmlNodeList stocks = xmlDoc.GetElementsByTagName("Billing");
            XmlNodeList Address = xmlDoc.GetElementsByTagName("Shipping");
            //XmlNodeList City = xmlDoc.GetElementsByTagName("City");
            //XmlNodeList State = xmlDoc.GetElementsByTagName("State");
            //XmlNodeList Zip = xmlDoc.GetElementsByTagName("Zip");

            // Now you can check each node of the list
            foreach (XmlNode node in stocks)
            {
                Console.WriteLine(node.InnerText);
            }
            foreach (XmlNode node in Address)
            {
                Console.WriteLine(node.InnerText);
            }
            //foreach (XmlNode node in City)
            //{
            //    Console.WriteLine(node.InnerText);
            //}
            //foreach (XmlNode node in State)
            //{
            //    Console.WriteLine(node.InnerText);
            //}
            //foreach (XmlNode node in Zip)
            //{
            //    Console.WriteLine(node.InnerText);
            //}



        }
    }
}
