﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JSONDemo.Startup))]
namespace JSONDemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
