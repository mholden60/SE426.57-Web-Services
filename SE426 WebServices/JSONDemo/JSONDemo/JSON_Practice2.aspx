﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
   
    <script type="text/javascript">
        function myData()
        {
        var url = 'http://carma.org/api/1.1/searchLocations?name=' + myForm.txtState.value;

        // Cross platform xmlhttprequest

        // Create xmlhttprequest object
        var xmlhttp = null;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
            //make sure that Browser supports overrideMimeType
            if (typeof xmlhttp.overrideMimeType != 'undefined') { xmlhttp.overrideMimeType('text/xml'); }
        } else if (window.ActiveXObject) {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } else {
            alert('Perhaps your browser does not support xmlhttprequests?');
        }

        // Create an HTTP GET request
        xmlhttp.open('GET', url, true);

        // Set the callback function
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                var myObj = JSON.parse(xmlhttp.responseText);
                for (i = 0; i < myObj.length; i++)
                {
                    myForm.txtName.value = myObj[i].name;
                    myForm.txtCount.value = myObj[i].plant_count;
                    myForm.txtPast.value = myObj[i].carbon.past;
                    myForm.txtPresent.value = myObj[i].carbon.present;
                    myForm.txtFuture.value = myObj[i].carbon.future;
                }
            } else {
                // waiting for the call to complete
            }
        };

        // Make the actual request
        xmlhttp.send(null);





}
    </script>
    
</head>
<body>
    <form id="myForm">
        Name: <textarea id="txtName" cols="20" rows="2"></textarea><br />
       Plant Count: <textarea id="txtCount" cols="20" rows="2"></textarea><br />
        Carbon Past: <textarea id="txtPast" cols="20" rows="2"></textarea><br />
        Carbon Present: <textarea id="txtPresent" cols="20" rows="2"></textarea><br />
        Carbon Future: <textarea id="txtFuture" cols="20" rows="2"></textarea><br />
        Enter State Name: <textarea id="txtState" cols="20" rows="2"></textarea><br />
        <input id="Button1" type="button" value="Get Data" onclick="myData()" />
    </form>
</body>
</html>

