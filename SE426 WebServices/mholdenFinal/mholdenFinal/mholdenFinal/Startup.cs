﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mholdenFinal.Startup))]
namespace mholdenFinal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
