﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace mholdenLabs2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBilling_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            String rootPath = Server.MapPath("~");
            string strFile = rootPath + "\\OrderInfo.xml";
            docNav = new XPathDocument(strFile);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("/Order/BillingInformation");

            while (NodeIter.MoveNext())
            {
                XPathNodeIterator billingInfo = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtBilling.Text = txtBilling.Text + billingInfo.Current.SelectSingleNode("Name") + " " + Environment.NewLine;
                txtBilling.Text = txtBilling.Text + billingInfo.Current.SelectSingleNode("Address") + " " + Environment.NewLine;
                txtBilling.Text = txtBilling.Text + billingInfo.Current.SelectSingleNode("City") + " " + Environment.NewLine;
                txtBilling.Text = txtBilling.Text + billingInfo.Current.SelectSingleNode("State") + " " + Environment.NewLine;
                txtBilling.Text = txtBilling.Text + billingInfo.Current.SelectSingleNode("Zip") + " " + Environment.NewLine;
            }
        }
        protected void btnShipping_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            String rootPath = Server.MapPath("~");
            string strFile = rootPath + "\\OrderInfo.xml";
            docNav = new XPathDocument(strFile);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("/Order/ShippingInformation");

            while (NodeIter.MoveNext())
            {
                XPathNodeIterator billingInfo = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtShipping.Text = txtShipping.Text + billingInfo.Current.SelectSingleNode("Name") + " " + Environment.NewLine;
                txtShipping.Text = txtShipping.Text + billingInfo.Current.SelectSingleNode("Address") + " " + Environment.NewLine;
                txtShipping.Text = txtShipping.Text + billingInfo.Current.SelectSingleNode("City") + " " + Environment.NewLine;
                txtShipping.Text = txtShipping.Text + billingInfo.Current.SelectSingleNode("State") + " " + Environment.NewLine;
                txtShipping.Text = txtShipping.Text + billingInfo.Current.SelectSingleNode("Zip") + " " + Environment.NewLine;
            }
        }

        protected void btnItem_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            String rootPath = Server.MapPath("~");
            string strFile = rootPath + "\\OrderInfo.xml";
            docNav = new XPathDocument(strFile);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("/Order/Items/Item");

            while (NodeIter.MoveNext())
            {
                XPathNodeIterator iteminfo = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("PartNo") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("Description") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("UnitPrice") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("Quantity") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("TotalCost") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("CustomerOptions/Size") + " " + Environment.NewLine;
                txtItem.Text = txtItem.Text + iteminfo.Current.SelectSingleNode("CustomerOptions/Color") + " " + Environment.NewLine;

            }
            txtNumber.Text = txtNumber.Text + nav.Evaluate("sum(//Order/Items/Item/Quantity)");
        }

        protected void txtNumber_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnTotal_Click1(object sender, EventArgs e)
            {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            String rootPath = Server.MapPath("~");
            string strFile = rootPath + "\\OrderInfo.xml";
            docNav = new XPathDocument(strFile);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("/Order/Items/Item");
            txtNumber.Text = txtNumber.Text + nav.Evaluate("sum(//Order/Items/Item/Quantity)");
        }

        protected void btnCost_Click(object sender, EventArgs e)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            String rootPath = Server.MapPath("~");
            string strFile = rootPath + "\\OrderInfo.xml";
            docNav = new XPathDocument(strFile);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("/Order/Items/Item");
            txtTotal.Text = txtTotal.Text + "$" + nav.Evaluate("sum(//Order/Items/Item/TotalCost)");
        }
    }
}