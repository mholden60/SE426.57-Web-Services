﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml.XPath;

namespace mholdenLab4a
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class OrderServices : IOrdersServices
    {

        public BillingAdd GetBillingAddressForAnOrder(int OrderID)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;


            string directory = HttpContext.Current.Server.MapPath(".");
          

            string strFilename = directory + "\\" + "OrderInfoLab3.xml";
            docNav = new XPathDocument(strFilename);
            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("//OrderFeed/Order[@id = "+ OrderID+"]/BillingInformation");
            BillingAdd bInfo = new BillingAdd();

            //string billin = nav.Select("//BillingInformation").ToString();
            while(NodeIter.MoveNext())
            {
                bInfo = new BillingAdd();
                bInfo.Name = Convert.ToString(NodeIter.Current.SelectSingleNode("Name"));
                bInfo.Address = Convert.ToString(NodeIter.Current.SelectSingleNode("Address"));
                bInfo.City = Convert.ToString(NodeIter.Current.SelectSingleNode("City"));
                bInfo.State = Convert.ToString(NodeIter.Current.SelectSingleNode("State"));
                bInfo.ZipCode = Convert.ToString(NodeIter.Current.SelectSingleNode("ZipCode"));
            }
            return bInfo;
            
        }

        public List<OrderInfo> GetItemListForOrder(int OrderID)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            string directory = HttpContext.Current.Server.MapPath(".");
            string strFilename = directory + "\\" + "OrderInfoLab3.xml";
            docNav = new XPathDocument(strFilename);
            nav = docNav.CreateNavigator();


            NodeIter = nav.Select("//OrderFeed/Order[@id="+OrderID+"]/Items/Item");

            OrderInfo itemList;
            List<OrderInfo> itemInfo = new List<OrderInfo>();
            while(NodeIter.MoveNext())
            {
                itemList = new OrderInfo();
                itemList.PartNo = Convert.ToString(NodeIter.Current.SelectSingleNode("PartNo"));
                itemList.Description = Convert.ToString(NodeIter.Current.SelectSingleNode("Description"));
                itemList.UnitPrice = Convert.ToString(NodeIter.Current.SelectSingleNode("UnitPrice"));
                itemList.Quantity = Convert.ToString(NodeIter.Current.SelectSingleNode("Quantity"));
                itemList.TotalCost = Convert.ToString(NodeIter.Current.SelectSingleNode("TotalCost"));
                itemList.CustomerOptions = Convert.ToString(NodeIter.Current.SelectSingleNode("CustomerOptions"));

                itemInfo.Add(itemList);
            }


            return itemInfo;
        }

        public int GetNumberofOrders()
        {
            XPathNavigator nav;
            XPathDocument docNav;

            string directory = HttpContext.Current.Server.MapPath(".");
            string strFilename = directory + "\\" + "OrderInfoLab3.xml";
            docNav = new XPathDocument(strFilename);

            nav = docNav.CreateNavigator();

            
            return nav.Select("//Order").Count;
        }

        public double GetTotalCostForAnOrder(int OrderId)
        {
            XPathNavigator nav;
            XPathDocument docNav;

            string directory = HttpContext.Current.Server.MapPath(".");
            string strFilename = directory + "\\" + "OrderInfoLab3.xml";
            docNav = new XPathDocument(strFilename);

            nav = docNav.CreateNavigator();
            double totalCost =Convert.ToDouble(nav.Evaluate("sum(//Order[@id="+ OrderId +"]//TotalCost)"));
            return totalCost;
        }

        public string HowManyOrderedForAPartNo(string sPartNo)
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;

            string directory = HttpContext.Current.Server.MapPath(".");
            string strFilename = directory + "\\" + "OrderInfoLab3.xml";
            docNav = new XPathDocument(strFilename);

            nav = docNav.CreateNavigator();

            NodeIter = nav.Select("//PartNo[.='" + sPartNo + "']/..");
            double total = 0;
            while(NodeIter.MoveNext())
                {
                 total = Convert.ToDouble(nav.Evaluate("sum(//PartNo[.='"+ sPartNo +"']/../Quantity)"));
                }

            string partInfo = sPartNo + " = " + total.ToString();
            return partInfo;
            
        }
    }
}
