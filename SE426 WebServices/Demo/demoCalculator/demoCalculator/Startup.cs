﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(demoCalculator.Startup))]
namespace demoCalculator
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
