﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mholdenLabs2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtBilling" runat="server" Height="16px" Width="374px"></asp:TextBox>
        <asp:Button ID="btnBilling" runat="server" Text="Billing Info" OnClick="btnBilling_Click" />
    </div>
        <div>

        </div>
    <div>
        <asp:TextBox ID="txtShipping" runat="server" Height="16px" Width="374px"></asp:TextBox>
        <asp:Button ID="btnShipping" runat="server" Text="Shipping Info" OnClick="btnShipping_Click" />
    </div>
        <div>

        </div>
    <div>
        <asp:TextBox ID="txtItem" runat="server" Height="86px" Width="440px" TextMode="MultiLine"></asp:TextBox>

        <asp:Button ID="btnItem" runat="server" OnClick="btnItem_Click" Text="Item Info" />

    </div>
        <div>
            <asp:TextBox ID="txtNumber" runat="server" OnTextChanged="txtNumber_TextChanged"></asp:TextBox>
            <asp:Button ID="btnTotal" runat="server" OnClick="btnTotal_Click1" Text="Item Total" />
        </div>
        <div>
            <asp:TextBox ID="txtTotal" runat="server"></asp:TextBox>
            <asp:Button ID="btnCost" runat="server" OnClick="btnCost_Click" Text="Item Cost" />
        </div>
    </form>
</body>
</html>
