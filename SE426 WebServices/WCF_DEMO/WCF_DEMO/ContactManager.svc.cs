﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCF_DEMO
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : iContactManager
    {

        public int GetNumberOfContacts ()
        {
         
             return 5;
 
        }

        public ContactInformation GetClientInformation()
        {
            ContactInformation newContact = new ContactInformation();

            newContact.Firstname = "Bruce";
            newContact.Lastname = "Ganek";
            newContact.PhoneNumber = "555-1212";
            newContact.FaxNumber = "111-1212";
            newContact.EmailAddress = "test@gmail.com";

            return newContact;

        }

    }
}
