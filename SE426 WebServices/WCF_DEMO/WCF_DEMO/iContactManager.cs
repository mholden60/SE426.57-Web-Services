﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCF_DEMO
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface iContactManager
    { 

        

        [OperationContract]
        int GetNumberOfContacts();

        [OperationContract]
        ContactInformation GetClientInformation();       
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class ContactInformation
    {
        string firstName;
        string lastName;
        string phoneNumber;
        string faxNumber;
        string emailAddress;

        
        [DataMember]
        public string Firstname
        {
            get { return firstName; }
            set { firstName = value; }
        }
        [DataMember]
        public string Lastname
        {
            get { return lastName; }
            set { lastName = value; }
        }
        [DataMember]
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }
        [DataMember]
        public string FaxNumber
        {
            get { return faxNumber; }
            set { faxNumber = value; }
        }
        [DataMember]
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
    }

    
}
