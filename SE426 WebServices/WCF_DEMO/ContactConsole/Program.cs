﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ContactReference.iContactManagerClient proxy = new ContactReference.iContactManagerClient();
            ContactReference.ContactInformation contact = proxy.GetClientInformation();
            Console.WriteLine(proxy.GetNumberOfContacts());
            Console.WriteLine(contact.Firstname);
            Console.WriteLine(contact.Lastname);
            Console.ReadLine();
        }
    }
}
