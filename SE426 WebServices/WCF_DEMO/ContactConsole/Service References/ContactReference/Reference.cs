﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ContactConsole.ContactReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ContactInformation", Namespace="http://schemas.datacontract.org/2004/07/WCF_DEMO")]
    [System.SerializableAttribute()]
    public partial class ContactInformation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EmailAddressField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FaxNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FirstnameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LastnameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PhoneNumberField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EmailAddress {
            get {
                return this.EmailAddressField;
            }
            set {
                if ((object.ReferenceEquals(this.EmailAddressField, value) != true)) {
                    this.EmailAddressField = value;
                    this.RaisePropertyChanged("EmailAddress");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FaxNumber {
            get {
                return this.FaxNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.FaxNumberField, value) != true)) {
                    this.FaxNumberField = value;
                    this.RaisePropertyChanged("FaxNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Firstname {
            get {
                return this.FirstnameField;
            }
            set {
                if ((object.ReferenceEquals(this.FirstnameField, value) != true)) {
                    this.FirstnameField = value;
                    this.RaisePropertyChanged("Firstname");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Lastname {
            get {
                return this.LastnameField;
            }
            set {
                if ((object.ReferenceEquals(this.LastnameField, value) != true)) {
                    this.LastnameField = value;
                    this.RaisePropertyChanged("Lastname");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PhoneNumber {
            get {
                return this.PhoneNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.PhoneNumberField, value) != true)) {
                    this.PhoneNumberField = value;
                    this.RaisePropertyChanged("PhoneNumber");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ContactReference.iContactManager")]
    public interface iContactManager {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/iContactManager/GetNumberOfContacts", ReplyAction="http://tempuri.org/iContactManager/GetNumberOfContactsResponse")]
        int GetNumberOfContacts();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/iContactManager/GetNumberOfContacts", ReplyAction="http://tempuri.org/iContactManager/GetNumberOfContactsResponse")]
        System.Threading.Tasks.Task<int> GetNumberOfContactsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/iContactManager/GetClientInformation", ReplyAction="http://tempuri.org/iContactManager/GetClientInformationResponse")]
        ContactConsole.ContactReference.ContactInformation GetClientInformation();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/iContactManager/GetClientInformation", ReplyAction="http://tempuri.org/iContactManager/GetClientInformationResponse")]
        System.Threading.Tasks.Task<ContactConsole.ContactReference.ContactInformation> GetClientInformationAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface iContactManagerChannel : ContactConsole.ContactReference.iContactManager, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class iContactManagerClient : System.ServiceModel.ClientBase<ContactConsole.ContactReference.iContactManager>, ContactConsole.ContactReference.iContactManager {
        
        public iContactManagerClient() {
        }
        
        public iContactManagerClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public iContactManagerClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public iContactManagerClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public iContactManagerClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int GetNumberOfContacts() {
            return base.Channel.GetNumberOfContacts();
        }
        
        public System.Threading.Tasks.Task<int> GetNumberOfContactsAsync() {
            return base.Channel.GetNumberOfContactsAsync();
        }
        
        public ContactConsole.ContactReference.ContactInformation GetClientInformation() {
            return base.Channel.GetClientInformation();
        }
        
        public System.Threading.Tasks.Task<ContactConsole.ContactReference.ContactInformation> GetClientInformationAsync() {
            return base.Channel.GetClientInformationAsync();
        }
    }
}
