﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;

namespace weatherDirect
{
    public partial class Weather : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void txtBoston_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnWeather_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
            XmlTextReader myXMLReader = null;           //Declare XMLReader           
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;


            //Create Request
            string WeatherURL = "http://api.wunderground.com/api/d8d28c244bd31f85/conditions/q/MA/Boston.xml";

            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(WeatherURL);
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";
            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());

            docNav = new XPathDocument(myXMLReader);
            // Create a navigator to query with XPath.
            nav = docNav.CreateNavigator();
            NodeIter = nav.Select("//current_observation");

                while(NodeIter.MoveNext())
            {
                XPathNodeIterator weather = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtBoston.Text = txtBoston.Text + weather.Current.SelectSingleNode("display_location/longitude") + Environment.NewLine;
                txtBoston.Text = txtBoston.Text + weather.Current.SelectSingleNode("display_location/latitude") + Environment.NewLine;
                txtBoston.Text = txtBoston.Text + weather.Current.SelectSingleNode("feelslike_f");
            }



            
        }

        protected void btnDay_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
            XmlTextReader myXMLReader = null;           //Declare XMLReader           
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;


            //Create Request
            string WeatherURL = "http://api.wunderground.com/api/d8d28c244bd31f85/forecast/q/MA/Boston.xml";

            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(WeatherURL);
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";
            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());

            docNav = new XPathDocument(myXMLReader);
            // Create a navigator to query with XPath.
            nav = docNav.CreateNavigator();
            NodeIter = nav.Select("//forecast/txt_forecast/forecastdays/forecastday");

            while (NodeIter.MoveNext())
            {
                XPathNodeIterator weather = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtDay.Text = txtDay.Text + weather.Current.SelectSingleNode("title") + Environment.NewLine;
                txtDay.Text = txtDay.Text + weather.Current.SelectSingleNode("fcttext") + Environment.NewLine;
                
               
            }




        }

        protected void btnCities_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
            XmlTextReader myXMLReader = null;           //Declare XMLReader           
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;
            //Create Request
            string postalURL = " http://api.geonames.org/postalCodeSearch?postalcode=" + txtZip.Text +"&maxRows=10&username=mholden60";
            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(postalURL);
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";
            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());
            docNav = new XPathDocument(myXMLReader);
            // Create a navigator to query with XPath.
            nav = docNav.CreateNavigator();

            string searchstring = "//code";
            NodeIter = nav.Select(searchstring);
          

                while (NodeIter.MoveNext())
                {
                    XPathNodeIterator zip = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                    txtGetZip.Text = txtGetZip.Text + zip.Current.SelectSingleNode("postalcode") + "-"+
                     txtGetZip.Text + zip.Current.SelectSingleNode("name") + Environment.NewLine;
                }
            

        }

        protected void btnNear_Click(object sender, EventArgs e)
        {
            HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
            XmlTextReader myXMLReader = null;           //Declare XMLReader           
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator NodeIter;
            //Create Request
            string postalURL = " http://api.geonames.org/findNearbyPostalCodes?postalcode=" + txtZip.Text + "&maxRows=100&username=mholden60";
            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(postalURL);
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";
            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());
            docNav = new XPathDocument(myXMLReader);
            // Create a navigator to query with XPath.
            nav = docNav.CreateNavigator();

            string searchstring = "//code";
            NodeIter = nav.Select(searchstring);

            while (NodeIter.MoveNext())
            {
                XPathNodeIterator zip = NodeIter.Current.SelectChildren(XPathNodeType.Element);
                txtGetZip.Text = txtGetZip.Text + zip.Current.SelectSingleNode("postalcode") + "-"+
                  zip.Current.SelectSingleNode("name") + Environment.NewLine;

            }
            }
        }
    
}
