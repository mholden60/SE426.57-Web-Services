﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(weatherDirect.Startup))]
namespace weatherDirect
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
