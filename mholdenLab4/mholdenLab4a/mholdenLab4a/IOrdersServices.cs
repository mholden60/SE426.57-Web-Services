﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace mholdenLab4a
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IOrdersServices
    {

        [OperationContract]
        int GetNumberofOrders();

        [OperationContract]
        double GetTotalCostForAnOrder(int OrderId);

        [OperationContract]
        List<OrderInfo> GetItemListForOrder(int OrderID);

        [OperationContract]
        string HowManyOrderedForAPartNo(string sPartNo);

        [OperationContract]
        BillingAdd GetBillingAddressForAnOrder(int OrderID);



        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class OrderInfo
    {
        string partNo;
        string description;
        string unitPrice;
        string quantity;
        string totalCost;
        string customerOptions;

        [DataMember]
        public string PartNo
        {
            get { return partNo; }
            set { partNo = value; }
        }

        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        [DataMember]
        public string UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }
        [DataMember]
        public string Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        [DataMember]
        public string TotalCost
        {
            get { return totalCost; }
            set { totalCost = value; }
        }
        [DataMember]
        public string CustomerOptions
        {
            get { return customerOptions; }
            set { customerOptions = value; }
        }

    }
    [DataContract]
    public class BillingAdd
    {
        string name;
        string address;
        string city;
        string state;
        string zipcode;

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        [DataMember]
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        [DataMember]
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        [DataMember]
        public string State
        {
            get { return state; }
            set { state = value; }
        }
        [DataMember]
        public string ZipCode
        {
            get { return zipcode; }
            set { zipcode = value; }
        }
    }
}
    
