﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleResults
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Billing Information for Order 1");
            ServiceReference1.OrdersServicesClient proxy = new ServiceReference1.OrdersServicesClient();
            ServiceReference1.BillingAdd binfo = proxy.GetBillingAddressForAnOrder(1);

            Console.WriteLine("Name = "+ binfo.Name);
            Console.WriteLine("Address = " + binfo.Address);
            Console.WriteLine("City = " + binfo.City);
            Console.WriteLine("State = " + binfo.State);
            Console.WriteLine("ZipCode = " + binfo.ZipCode);

            Console.WriteLine("Number of Orders = " + proxy.GetNumberofOrders());
            Console.WriteLine("Total cost for order 1 = " + proxy.GetTotalCostForAnOrder(1));
            Console.WriteLine("The Number if Orders with part " + proxy.HowManyOrderedForAPartNo("JETSWEATER"));
            Console.WriteLine("Items in order 1 (Qty, Part, Description)");
            ServiceReference1.OrderInfo[] itemInfo = proxy.GetItemListForOrder(1);
            
            for(int x = 0; x < itemInfo.Count(); x++)
            {
                Console.WriteLine(itemInfo[x].Quantity + " " + itemInfo[x].PartNo + " " + itemInfo[x].Description);

            }
            //Console.WriteLine("Items in order1" + proxy.GetItemListForOrder;
            Console.WriteLine("Enter CR To Exit");
            Console.ReadLine();
        }
    }
}
